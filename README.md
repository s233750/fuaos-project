# FUAOS-PILE inspection
Welcome to the Fuaos Pile Inspection project repository! This project aims to extract the diameter of underwater piles using data collected from a BlueROV equipped with a front-looking sonar and a stereo camera. The goal is to utilize the calibration of the stereo camera, performed with the KALIBR toolbox, and employ the OpenCV toolbox to run feature matching algorithms to extract the dimensions of the piles visible in the video footage.

## Getting started
To get started with this repository, follow the steps below to add it to your desktop and sync it with your local files:

1. Clone the Repository: Clone this repository to your local machine using the following command in your terminal or command prompt:

```bash 
git clone <repository-url>
``` 

2. Navigate to Repository Directory: Change directory to the cloned repository folder:

``` bash
cd <repository-folder>
```

3. Sync Repository: Ensure that your local repository is synchronized with the remote repository by pulling the latest changes:

``` bash
git pull origin master
```

Now you're all set to start working with the project files!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.gbar.dtu.dk/s233750/fuaos-pile-inspection.git
git branch -M main
git push -uf origin main
```
## Project Overview

The Fuaos Pile Inspection project focuses on analyzing data collected from a BlueROV with a front-looking sonar and a stereo camera. The main objective is to extract the diameter of underwater piles visible in the video footage obtained from the stereo camera.

## Tools Used

- KALIBR Toolbox: Utilized for calibrating the stereo camera setup to obtain accurate depth and distance measurements.
- OpenCV Toolbox: Employed for implementing feature matching algorithms to extract the dimensions of the piles from the stereo camera footage.

## File Structure

The repository is organized as follows:

- calibration/: Contains files and scripts related to the calibration process using the KALIBR toolbox.
- feature_matching/: Includes files and scripts implementing feature matching algorithms using OpenCV for pile diameter extraction.
- data/: Stores the collected data from the BlueROV, including video footage and sonar data.

## Contribution Guidelines

If you'd like to contribute to the project, feel free to fork this repository, make your changes, and submit a pull request. Please adhere to the following guidelines:

- Make sure your code follows the project's coding conventions and standards.
- Provide clear and descriptive commit messages.
- If you're adding new features or making significant changes, consider updating the documentation accordingly.

## Contact Information 

For any questions, feedback, or concerns, please contact the project maintainers:

- Laurin Bachmann  
- Jonas Bolduan
- Riccardo Di Perna
- Rémi Larnouhet
- Emma Dubois

We appreciate your interest in contributing to the Fuaos Pile Inspection project!


# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

